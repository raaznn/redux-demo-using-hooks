import React, { useState } from "react";

//redux hooks | useSelector selects required state | useDispatch dispatches action in store
import { useSelector, useDispatch } from "react-redux";

//redux action creator functions | returns objects with property "type"
import { addRoom, removeRoom } from "../Redux/Room/RoomActions";

//===========================================================================

function RoomComponent() {
  const [room, setRoom] = useState("");
  const { roomList } = useSelector((state) => state.room);
  const dispatch = useDispatch();

  //update room when input changes
  const handleChange = (room) => {
    setRoom(room);
  };

  //add new room to the roomList
  const handleAdd = (e) => {
    e.preventDefault();
    const id = Date.now();
    room && dispatch(addRoom(id, room));
    setRoom("");
  };

  //remove room from roomList
  const handleRemove = (id) => {
    dispatch(removeRoom(id));
  };

  //===========================================================================

  return (
    <div style={{ marginRight: "50px" }}>
      <h3>Room Rent</h3>
      <form onSubmit={handleAdd}>
        <input
          type="text"
          value={room}
          onChange={(e) => handleChange(e.target.value)}
          placeholder="Add Room"
        ></input>
        <input type="submit" value="ADD" />
      </form>

      <div>
        {roomList.map((r) => {
          return (
            <div
              key={r.id}
              style={{
                display: "flex",
                justifyContent: "space-between",
                marginTop: "10px",
              }}
            >
              <span>{r.name}</span>
              <span
                onClick={() => handleRemove(r.id)}
                style={{ color: "red", cursor: "pointer" }}
              >
                X
              </span>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default RoomComponent;
