import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addFlat, removeFlat } from "../Redux/Flat/FlatActions";

function FlatComponent() {
  const [flat, setflat] = useState("");
  const [noRooms, setNoRooms] = useState("");

  const { flatList } = useSelector((state) => state.flat);
  const dispatch = useDispatch();

  const handleFlatChange = (flat) => {
    setflat(flat);
  };
  const handleNoRoomsChange = (num) => {
    setNoRooms(num);
  };
  const handleAdd = (e) => {
    e.preventDefault();
    const id = Date.now();
    flat && noRooms && dispatch(addFlat(id, flat, noRooms));
    setflat("");
    setNoRooms("");
  };

  const handleRemove = (id) => {
    dispatch(removeFlat(id));
  };

  return (
    <div style={{ marginLeft: "50px" }}>
      <h3>Flat Rent</h3>

      <form onSubmit={handleAdd}>
        <input
          type="text"
          value={flat}
          onChange={(e) => handleFlatChange(e.target.value)}
          placeholder="Add Flat"
          style={{ width: "120px" }}
        />
        <input
          type="number"
          value={noRooms}
          onChange={(e) => handleNoRoomsChange(e.target.value)}
          placeholder="No of rooms"
          style={{ width: "90px" }}
        />
        <input type="submit" value="ADD" />
      </form>
      <div>
        {flatList.map((f) => {
          return (
            <div
              key={f.id}
              style={{
                display: "flex",
                justifyContent: "space-between",
                marginTop: "10px",
              }}
            >
              <span>{`${f.name} (R-${f.noRooms})`}</span>
              <span
                onClick={() => handleRemove(f.id)}
                style={{ color: "red", cursor: "pointer" }}
              >
                X
              </span>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default FlatComponent;
