import "./App.css";
import FlatComponent from "./Components/FlatComponent";
import RoomComponent from "./Components/RoomComponent";

function App() {
  return (
    <div className="App-header">
      <h2>Redux Demo</h2>
      <div style={{ display: "flex" }}>
        <RoomComponent />
        <FlatComponent />
      </div>
    </div>
  );
}

export default App;
