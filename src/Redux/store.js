import { combineReducers, createStore } from "redux";

//reducers
import roomReducer from "./Room/RoomReducer";
import flatReducer from "./Flat/FlatReducer";

//======================================================================

const rootReducer = combineReducers({ room: roomReducer, flat: flatReducer });

const store = createStore(rootReducer);

export default store;
