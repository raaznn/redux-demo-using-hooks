export const ADD_ROOM = "ADD_ROOM";
export const REMOVE_ROOM = "REMOVE_ROOM";

export const addRoom = (id, name) => {
  return {
    type: ADD_ROOM,
    payload: {
      id,
      name,
    },
  };
};

export const removeRoom = (id) => {
  return {
    type: REMOVE_ROOM,
    payload: id,
  };
};
