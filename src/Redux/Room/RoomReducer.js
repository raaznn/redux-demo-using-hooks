import { ADD_ROOM, REMOVE_ROOM } from "./RoomActions";

const initialState = {
  roomList: [
    {
      id: 1,
      name: "Gaindakot",
    },
    {
      id: 2,
      name: "Kawasoti",
    },
    {
      id: 3,
      name: "Naryanghar",
    },
  ],
};

const roomReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ROOM:
      return { ...state, roomList: [action.payload, ...state.roomList] };
    case REMOVE_ROOM:
      return {
        ...state,
        roomList: state.roomList.filter((r) => r.id !== action.payload),
      };
    default:
      return state;
  }
};

export default roomReducer;
