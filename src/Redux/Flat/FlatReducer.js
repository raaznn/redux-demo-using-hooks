import { ADD_FLAT, REMOVE_FLAT } from "./FlatActions";

const initialState = {
  flatList: [
    {
      id: 1,
      name: "Bharatpur",
      noRooms: 3,
    },
    {
      id: 2,
      name: "Danda",
      noRooms: 4,
    },
  ],
};

const flatReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_FLAT:
      return { ...state, flatList: [action.payload, ...state.flatList] };
    case REMOVE_FLAT:
      return {
        ...state,
        flatList: state.flatList.filter((r) => r.id !== action.payload),
      };
    default:
      return state;
  }
};

export default flatReducer;
