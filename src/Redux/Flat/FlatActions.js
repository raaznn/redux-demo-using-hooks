export const ADD_FLAT = "ADD_FLAT";
export const REMOVE_FLAT = "REMOVE_FLAT";

export const addFlat = (id, name, noRooms) => {
  return {
    type: ADD_FLAT,
    payload: {
      id,
      name,
      noRooms,
    },
  };
};

export const removeFlat = (id) => {
  return {
    type: REMOVE_FLAT,
    payload: id,
  };
};
